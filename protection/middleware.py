from django.utils.deprecation import MiddlewareMixin

from logs.models import Log


class LoggingMiddleware(MiddlewareMixin):
    """
    Middleware for logs user activity in website
    """
    def process_request(self, request):
        user_agent = request.META.get('HTTP_USER_AGENT')
        if request.user.is_authenticated:
            Log.objects.create(user_agent=user_agent, user=request.user)
