from django.conf.urls import url, include

from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

from api.v1.entries.views import EntryCreateView, EntryDetailView, EntryStatView

schema_view = get_swagger_view(title='Protection API - v1')

router = routers.DefaultRouter()
router.register(r'entry/create', EntryCreateView)
router.register(r'entry/detail', EntryDetailView)

urlpatterns = [
    url(r'^$', schema_view),
    url(r'^', include(router.urls)),
    url(r'^entry/stat/', EntryStatView.as_view(), name='entry_stat'),
]
