from rest_framework import mixins, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from api.v1.entries.serializers import EntrySerializer
from entries.models import Entry


class EntryCreateView(mixins.CreateModelMixin, GenericViewSet):
    queryset = Entry.objects.active()
    serializer_class = EntrySerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class EntryDetailView(mixins.RetrieveModelMixin, mixins.CreateModelMixin, GenericViewSet):
    queryset = Entry.objects.active()
    serializer_class = EntrySerializer
    lookup_field = 'slug'

    def get_object(self):
        slug = self.kwargs.get('slug')
        return Entry.objects.filter(slug=slug).first()

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance:
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)

    def create(self, request, *args, **kwargs):
        """
        INFO:
        create/post method is used only for getting access to entry object
        without any data saving and other database changes
        """

        slug = self.request.data.get('slug')
        password = self.request.data.get('password')

        try:
            entry = Entry.objects.get(slug=slug, password=password)
            entry.raise_counter()
        except Entry.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if entry.document:
            return Response({'document': entry.document.url}, status=status.HTTP_200_OK)

        if entry.link:
            return Response({'link': entry.link}, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_404_NOT_FOUND)


class EntryStatView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        entries = Entry.objects.all()
        entry_date_list = entries.values_list('created', flat=True)
        entry_date_list = set(map(lambda x: x.strftime('%Y-%m-%d'), entry_date_list))

        data = {}
        for date in entry_date_list:
            year, month, day = date.split('-')
            query = entries.filter(created__year=year, created__month=month, created__day=day)
            documents = query.documents().filter(counter__gt=0).count()
            links = query.links().filter(counter__gt=0).count()
            if documents or links:
                data.update({date: {'files': documents, 'links': links}})

        return Response(data, status=status.HTTP_200_OK)
