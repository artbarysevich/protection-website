import datetime

from django.contrib.auth.models import User
from rest_framework.test import APITestCase

from entries.models import Entry


class EntryStatViewTest(APITestCase):

    def setUp(self):
        self.stat_url = '/api/v1/entry/stat/'
        self.user = User.objects.create_user('test', 'test@test.com', 'test')

        self.client.login(username='test', password='test')

        self.today = datetime.datetime.today()
        self.yesterday = self.today - datetime.timedelta(days=1)
        self.tomorrow = self.today + datetime.timedelta(days=1)

    def test_statistic_with_zero_counter_entries(self):
        Entry.objects.create(link='http://google.com/test/', user=self.user)
        Entry.objects.create(link='http://google.com/test/', user=self.user)
        Entry.objects.create(link='http://google.com/test/', user=self.user)
        response = self.client.get(self.stat_url)
        self.assertEqual(len(response.data), 0)

    def test_statistic_with_positive_counter_entries(self):
        entry_yesterday = Entry.objects.create(link='http://google.com/test/', user=self.user)
        entry_yesterday.raise_counter()
        entry_yesterday.created = self.yesterday
        entry_yesterday.save()
        response = self.client.get(self.stat_url)
        self.assertEqual(len(response.data), 1)

        entry_today = Entry.objects.create(link='http://google.com/test/', user=self.user)
        entry_today.raise_counter()
        entry_today.created = self.today
        entry_today.save()
        response = self.client.get(self.stat_url)
        self.assertEqual(len(response.data), 2)

        entry_tomorrow = Entry.objects.create(link='http://google.com/test/', user=self.user)
        entry_tomorrow.raise_counter()
        entry_tomorrow.created = self.tomorrow
        entry_tomorrow.save()
        response = self.client.get(self.stat_url)
        self.assertEqual(len(response.data), 3)
