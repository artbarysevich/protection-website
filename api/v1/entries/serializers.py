from rest_framework import serializers

from django.utils.translation import ugettext_lazy as _

from entries.models import Entry


class EntrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Entry
        fields = ('pk', 'link', 'document', 'password', 'slug', 'counter', 'created')
        read_only_fields = ('pk', 'password', 'slug', 'counter', 'created')

    def validate(self, data):
        link = data.get('link')
        document = data.get('document')

        if not link and not document:
            raise serializers.ValidationError(_('You should fill `document` field or `link` field'))

        if link and document:
            raise serializers.ValidationError(_('You must select only one type of protected data'))

        return data

