from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Log(models.Model):
    """
    Saving logs to SQL databases is not good idea.
    MongoDB is better way for storage this data formats.
    """
    user_agent = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Log')
        verbose_name_plural = _('Logs')

