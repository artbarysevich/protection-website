from django.contrib import admin

from logs.models import Log


class LogAdmin(admin.ModelAdmin):
    fields = ('user_agent', 'created_at', 'user')
    readonly_fields = ('user_agent', 'created_at', 'user')


admin.site.register(Log, LogAdmin)
