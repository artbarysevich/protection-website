from django.http import Http404

from entries.models import Entry


class ActiveEntryMixin(object):
    """
    Provides access only to Entries that have the correct date (last 24 hours added objects)
    """

    def dispatch(self, request, *args, **kwargs):
        slug = kwargs.get('slug')
        if not Entry.objects.active().filter(slug=slug).exists():
            raise Http404
        return super(ActiveEntryMixin, self).dispatch(request, *args, **kwargs)