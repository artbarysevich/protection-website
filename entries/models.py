from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.crypto import get_random_string

from django_extensions.db.models import TimeStampedModel

from entries.managers import EntryManager


class Entry(TimeStampedModel):
    link = models.URLField(_('Link'), blank=True, null=True)
    document = models.FileField(_('Document/File'), blank=True, null=True)
    slug = models.SlugField(_('Slug'), unique=True)
    password = models.CharField(_('Password'), max_length=24)
    counter = models.PositiveIntegerField(_('Counter'), default=0)

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    objects = EntryManager()

    class Meta:
        verbose_name = _('Entry')
        verbose_name_plural = _('Entries')

    def __str__(self):
        return str(self.pk)

    def save(self, **kwargs):
        if not self.slug:
            self.slug = self.generate_slug()
        if not self.password:
            self.password = self.generate_password()
        super(Entry, self).save(**kwargs)

    def raise_counter(self):
        self.counter += 1
        self.save()

    @staticmethod
    def generate_slug():
        while True:
            random_slug = get_random_string().lower()
            if not Entry.objects.filter(slug=random_slug).exists():
                return random_slug

    @staticmethod
    def generate_password():
        """
        Password in database should be encoded/hashed, but this password is generated
        automatically (not user private password) and here this not needed.
        """
        random_password = get_random_string().lower()
        return random_password

