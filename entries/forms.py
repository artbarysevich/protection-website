from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from entries.models import Entry


class EntryForm(forms.ModelForm):

    class Meta:
        model = Entry
        fields = ('link', 'document')

    def __init__(self, *args, **kwargs):
        super(EntryForm, self).__init__(*args, **kwargs)
        self.fields['link'].widget.attrs['class'] = 'form-control'
        self.fields['document'].widget.attrs['class'] = 'form-control'

    def clean(self):
        link = self.cleaned_data.get('link')
        document = self.cleaned_data.get('document')

        if not link and not document:
            raise ValidationError(_('You should fill `document` field or `link` field'))

        if link and document:
            raise ValidationError(_('You must select only one type of protected data'))

        return self.cleaned_data


class EntryAccessForm(forms.Form):
    password = forms.CharField()  # or forms.PasswordInput()

    def __init__(self, *args, **kwargs):
        super(EntryAccessForm, self).__init__(*args, **kwargs)
        self.fields['password'].widget.attrs['class'] = 'form-control'
