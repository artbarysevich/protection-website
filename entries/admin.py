from django.contrib import admin

from entries.models import Entry


class EntryAdmin(admin.ModelAdmin):
    readonly_fields = ('document', 'link', 'user', 'counter', 'slug')


admin.site.register(Entry, EntryAdmin)
