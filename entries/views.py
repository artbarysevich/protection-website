from django.http import FileResponse, HttpResponseRedirect
from django.views.generic import FormView
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.mixins import LoginRequiredMixin

from entries.forms import EntryForm, EntryAccessForm
from entries.mixins import ActiveEntryMixin
from entries.models import Entry


class EntryFormView(LoginRequiredMixin, FormView):
    form_class = EntryForm
    template_name = 'entries/entry_form.html'

    def form_valid(self, form):
        form.instance.user = self.request.user
        entry = form.save()
        context = self.get_context_data()
        context.update({'entry': entry})
        return self.render_to_response(context=context)


class EntryDetailView(ActiveEntryMixin, FormView):
    form_class = EntryAccessForm
    template_name = 'entries/entry_detail.html'

    def get(self, request, *args, **kwargs):
        context_data = self.get_context_data()
        entry = kwargs.get('entry')
        password_error = kwargs.get('password_error')

        if entry:
            context_data.update({'entry': entry})
        elif password_error:
            context_data.update({'password_error_message': _('Password is incorrect')})

        return self.render_to_response(context_data)

    def post(self, request, *args, **kwargs):
        password = request.POST.get('password')
        slug = kwargs.get('slug')

        try:
            entry = Entry.objects.get(slug=slug, password=password)
            entry.raise_counter()
            kwargs.update({'entry': entry})
        except Entry.DoesNotExist:
            kwargs.update({'password_error': True})

        return self.get(request, *args, **kwargs)

    def render_to_response(self, context, **response_kwargs):
        entry = context.get('entry')

        # if entry is link then redirect to this link
        if entry and entry.link:
            return HttpResponseRedirect(entry.link)

        # if entry is document then return this file in response
        if entry and entry.document:
            file_response = FileResponse(open(entry.document.path, 'rb'))
            file_response['Content-Disposition'] = 'attachment; filename={}'.format(entry.document.name)
            return file_response

        return super(EntryDetailView, self).render_to_response(context, **response_kwargs)
