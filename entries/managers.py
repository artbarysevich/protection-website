import datetime

from django.db import models


class EntryQuerySet(models.QuerySet):

    def active(self):
        """
        :return: objects that was added behind last 24 hours
        """
        time_24_hours_ago = datetime.datetime.now() - datetime.timedelta(days=1)
        return self.filter(created__gte=time_24_hours_ago)

    def documents(self):
        return self.exclude(document='')

    def links(self):
        return self.filter(link__isnull=False)


class EntryManager(models.Manager):

    def get_queryset(self):
        return EntryQuerySet(self.model, using=self._db)

    def active(self):
        return self.get_queryset().active()

    def documents(self):
        return self.get_queryset().documents()

    def links(self):
        return self.get_queryset().links()
