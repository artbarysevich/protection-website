import os

import datetime
from django.contrib.auth.models import User
from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile

from entries.forms import EntryForm, EntryAccessForm
from entries.models import Entry

TEST_DIR = os.path.dirname(os.path.abspath(__file__))
TEST_DATA_DIR = os.path.join(TEST_DIR, 'tests_data')


class EntryFormTests(TestCase):

    def setUp(self):
        self.document_path = os.path.join(TEST_DATA_DIR, 'art.barysevich.pdf')

    def test_entry_create_form_with_empty_data(self):
        data = {}
        form = EntryForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue("You should fill `document` field or `link` field" in str(form.errors))

    def test_entry_create_form_with_wrong_url_field(self):
        data = {'link': 'some text'}
        form = EntryForm(data=data)
        self.assertTrue("Enter a valid URL" in str(form.errors))
        self.assertFalse(form.is_valid())

    def test_entry_create_form_with_two_filled_fields(self):
        with open(self.document_path, 'rb') as doc:
            data = {'link': 'http://google.com'}
            files = {'document': SimpleUploadedFile('document', doc.read())}
            form = EntryForm(data=data, files=files)
            self.assertTrue('You must select only one type of protected data' in str(form.errors))
            self.assertFalse(form.is_valid())

    def test_entry_create_form_with_valid_data(self):
        data = {'link': 'http://google.com/test/page/'}
        form = EntryForm(data=data)
        self.assertTrue(form.is_valid())

        with open(self.document_path, 'rb') as doc:
            files = {'document': SimpleUploadedFile('document', doc.read())}
            form = EntryForm(files=files)
            self.assertTrue(form.is_valid())

    def test_entry_access_form(self):
        data = {}
        form = EntryAccessForm(data=data)
        self.assertFalse(form.is_valid())

        data = {'password': '123456'}
        form = EntryAccessForm(data=data)
        self.assertTrue(form.is_valid())


class EntryModelTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('test', 'test@test.com', 'test')

    def test_entry_model_raise_counter_method(self):
        entry = Entry.objects.create(link='http://google.com', user=self.user)
        self.assertEqual(entry.counter, 0)
        entry.raise_counter()
        self.assertTrue(entry.counter, 1)

    def test_entry_model_password_and_slug_fields(self):
        entry = Entry.objects.create(link='http://google.com', user=self.user)
        self.assertTrue(len(entry.password) > 5)
        self.assertTrue(len(entry.slug) > 5)

    def test_entry_model_active_objects(self):
        entry = Entry.objects.create(link='http://google.com', user=self.user)
        self.assertEqual(Entry.objects.active().count(), 1)

        new_date = datetime.datetime.today() - datetime.timedelta(hours=23)
        entry.created = new_date
        entry.save()
        self.assertEqual(Entry.objects.active().count(), 1)

        new_date = datetime.datetime.today() - datetime.timedelta(hours=25)
        entry.created = new_date
        entry.save()
        self.assertEqual(Entry.objects.active().count(), 0)


class EntryViewTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('test', 'test@test.com', 'test')
        self.entry = Entry.objects.create(link='http://google.com', user=self.user)

    def test_entry_create_view_access_to_page(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/login/?next=/')

        # login
        self.client.login(username='test', password='test')

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('id="id_link"' in str(response.content))
        self.assertTrue('id="id_document"' in str(response.content))

    def test_entry_detail_view_page(self):
        detail_url = '/{}/'.format(self.entry.slug)

        response = self.client.get(detail_url)
        self.assertTrue(response.status_code, 200)
        self.assertTrue('id="id_password"' in str(response.content))

        response = self.client.post(detail_url, {'password': 'wrong-password'})
        self.assertTrue('Password is incorrect' in str(response.content))

        response = self.client.post(detail_url, {'password': self.entry.password})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, self.entry.link)
